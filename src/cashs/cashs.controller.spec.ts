import { Test, TestingModule } from '@nestjs/testing';
import { CashsController } from './cashs.controller';
import { CashsService } from './cashs.service';

describe('CashsController', () => {
  let controller: CashsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CashsController],
      providers: [CashsService],
    }).compile();

    controller = module.get<CashsController>(CashsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
