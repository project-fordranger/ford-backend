import { Injectable } from '@nestjs/common';
import { CreateCashDto } from './dto/create-cash.dto';
import { UpdateCashDto } from './dto/update-cash.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Cash } from './entities/cash.entity';

@Injectable()
export class CashsService {
  constructor(
    @InjectRepository(Cash)
    private cashsRepository: Repository<Cash>
  ) {}

  create(createCashDto: CreateCashDto) {
    return this.cashsRepository.save(createCashDto);
  }

  findAll() {
    return this.cashsRepository.find();
  }

  findOne(id: number) {
    return this.cashsRepository.findOne({
      where: { id: id },
    });
  }

  update(id: number, updateCashDto: UpdateCashDto) {
    return `This action updates a #${id} cash`;
  }

  async remove(id: number) {
    const cash = await this.cashsRepository.findOneBy({ id: id });
    return this.cashsRepository.softRemove(cash);
  }
}
