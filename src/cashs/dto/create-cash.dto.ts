import { IsNotEmpty } from "class-validator";

export class CreateCashDto {
  
  @IsNotEmpty()
  cashsum: number;

  @IsNotEmpty()
  cashamount: number;

  @IsNotEmpty()
  cashchange: number;
}
