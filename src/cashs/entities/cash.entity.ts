import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';

@Entity()
export class Cash {
  @PrimaryGeneratedColumn({name: "cash_id"})
  id: number;

  @Column()
  cashsum: number;

  @Column()
  cashamount: number;

  @Column()
  cashchange: number;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updateAt: Date;

  @DeleteDateColumn()
  deleatedAt: Date;
}

