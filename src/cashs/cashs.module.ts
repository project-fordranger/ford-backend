import { Module } from '@nestjs/common';
import { CashsService } from './cashs.service';
import { CashsController } from './cashs.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cash } from './entities/cash.entity';

@Module({
  controllers: [CashsController],
  providers: [CashsService],
  imports: [TypeOrmModule.forFeature([Cash])],
})
export class CashsModule {}
