import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn({name: "user_id"})
  id: number;

  @Column({
    length: '32',
  })
  name: string;

  @Column()
  position: string;
  
  @Column({
    unique: true,
    length: '64',
  })
  email: string;
  @Column({
    length: '128',
  })
  password: string;

  @Column({
    length: '128',
    default: 'No-Image-Placeholder.jpg',
  })
  image: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
