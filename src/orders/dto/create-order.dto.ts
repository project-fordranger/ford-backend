class CreatedOrderItemDto {
  productId: number;
  amount: number;
}
export class CreateOrderDto {
  customerId: number;
  storeId: number;
  employeeId: number;
  orderItems: CreatedOrderItemDto[];
  discount: number;
}
