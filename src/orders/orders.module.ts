import { Module } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { OrdersController } from './orders.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Customer } from 'src/customers/entities/customer.entity';
import { Product } from 'src/products/entities/product.entity';
import { OrderItem } from './entities/order-item';
import { Order } from './entities/order.entity';
import { Store } from 'src/store/entities/store.entity';
import { Employee } from 'src/employees/entities/employee.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Order, OrderItem, Customer, Product , Store , Employee])],
  controllers: [OrdersController],
  providers: [OrdersService]
})
export class OrdersModule {}
