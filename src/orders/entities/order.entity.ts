import { Transform } from 'class-transformer';
import { Customer } from 'src/customers/entities/customer.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { OrderItem } from './order-item';
import { Employee } from 'src/employees/entities/employee.entity';
import { Store } from 'src/store/entities/store.entity';
@Entity()
export class Order {
  @PrimaryGeneratedColumn({name: "order_id"})
  id: number;

  @Column()
  amount: number;

  @Column({ type: 'float' })
  total: number;

  @Column({ type: 'float' ,default: 0})
  discount: number;

  @ManyToOne(() => Customer, (customer) => customer.orders)
  customer: Customer; // Customer Id

  @ManyToOne(() => Employee, (employee) => employee.orders)
  employee: Employee; // Employee Id

  @ManyToOne(() => Store, (store) => store.orders)
  store: Store; // Store Id

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  orderItems: OrderItem[];
}
