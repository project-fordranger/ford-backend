import { Module } from '@nestjs/common';
import { CreditsService } from './credits.service';
import { CreditsController } from './credits.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Creditcard } from './entities/credit.entity';

@Module({
  controllers: [CreditsController],
  providers: [CreditsService],
  imports: [TypeOrmModule.forFeature([Creditcard])],
})
export class CreditsModule {}
