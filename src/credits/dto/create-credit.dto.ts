import { IsNotEmpty } from "class-validator";

export class CreateCreditDto {
  @IsNotEmpty()
  creditname :string;

  @IsNotEmpty()
  creditnumber: string;

  @IsNotEmpty()
  creditamount: number;

}
