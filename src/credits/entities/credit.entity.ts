import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';

@Entity()
export class Creditcard {
  @PrimaryGeneratedColumn({name: "credit_id"})
  id: number;

  @Column()
  creditname :string;

  @Column()
  creditnumber: string;

  @Column()
  creditamount: number;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updateAt: Date;

  @DeleteDateColumn()
  deleatedAt: Date;
}

