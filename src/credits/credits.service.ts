import { Injectable } from '@nestjs/common';
import { CreateCreditDto } from './dto/create-credit.dto';
import { UpdateCreditDto } from './dto/update-credit.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Creditcard } from './entities/credit.entity';

@Injectable()
export class CreditsService {
  constructor(
    @InjectRepository(Creditcard)
    private credsRepository: Repository<Creditcard>
  ) {}

  create(createCreditDto: CreateCreditDto) {
    
    return this.credsRepository.save(createCreditDto);;
  }

  findAll() {
    return this.credsRepository.find();
  }

  findOne(id: number) {
    return this.credsRepository.findOne({
      where: { id: id },
    });
  }

  update(id: number, updateCreditDto: UpdateCreditDto) {
    return `This action updates a #${id} credit`;
  }

  remove(id: number) {
    return `This action removes a #${id} credit`;
  }
}
