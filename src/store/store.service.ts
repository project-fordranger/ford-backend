import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateStoreDto } from './dto/create-store.dto';
import { UpdateStoreDto } from './dto/update-store.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Store } from './entities/store.entity';
import { Repository } from 'typeorm';

@Injectable()
export class StoreService {
  constructor(
    @InjectRepository(Store)
    private StoresRepository: Repository<Store>,
  ) {}
  create(createStoreDto: CreateStoreDto) {
    return this.StoresRepository.save(createStoreDto);
  }

  findAll() {
    return this.StoresRepository.find({
      order: {
        id: 'DESC'
    }
  });
  }

  async findOne(id: number) {
    const store = await this.StoresRepository.findOne({
      where: { id: id },
      relations: ['orders'],
    });
    if (!store) {
      throw new NotFoundException();
    }
    return store;
  }

  async update(id: number, updateStoreDto: UpdateStoreDto) {
    const store = await this.StoresRepository.findOneBy({ id: id });
    if (!store) {
      throw new NotFoundException();
    }
    const updateStore = { ...store, ...updateStoreDto };
    return this.StoresRepository.save(updateStore);
  }
  async remove(id: number) {
    const store = await this.StoresRepository.findOneBy({ id: id });
    if (!store) {
      throw new NotFoundException();
    }
    return this.StoresRepository.softRemove(store);
  }
}
