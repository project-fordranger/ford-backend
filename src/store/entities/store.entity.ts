import { Order } from "src/orders/entities/order.entity";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Store {

  @PrimaryGeneratedColumn({name: "store_id"})
  id: number;

  @Column({
    length: '32',
  })
  name: string;

  @Column()
  tel: string;


  @Column({
    length: '32',
  })
  city: string;

  @Column({
    length: '32',
  })
  state: string;

  @OneToMany(() => Order, (order) => order.store)
  orders: Order[];
}
