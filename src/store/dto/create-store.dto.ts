export class CreateStoreDto {
  name: string;
  tel: string;
  city: string;
  state: string;
}
