import { Injectable } from '@nestjs/common';
import { CreateQrcodeDto } from './dto/create-qrcode.dto';
import { UpdateQrcodeDto } from './dto/update-qrcode.dto';
import { Qrcode } from './entities/qrcode.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class QrcodesService {
  constructor(
    @InjectRepository(Qrcode)
    private qrcodeRepository: Repository<Qrcode>
  ) {}

  create(createQrcodeDto: CreateQrcodeDto) {
    return this.qrcodeRepository.save(createQrcodeDto);;
  }

  findAll() {
    return this.qrcodeRepository.find();
  }

  findOne(id: number) {
    return this.qrcodeRepository.findOne({
      where: { id: id },
    });
  }

  update(id: number, updateQrcodeDto: UpdateQrcodeDto) {
    return `This action updates a #${id} qrcode`;
  }

  remove(id: number) {
    return `This action removes a #${id} qrcode`;
  }
}
