import { Module } from '@nestjs/common';
import { QrcodesService } from './qrcodes.service';
import { QrcodesController } from './qrcodes.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Qrcode } from './entities/qrcode.entity';

@Module({
  controllers: [QrcodesController],
  providers: [QrcodesService],
  imports: [TypeOrmModule.forFeature([Qrcode])],
})
export class QrcodesModule {}
