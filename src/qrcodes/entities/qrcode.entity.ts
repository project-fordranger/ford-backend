import { PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, Entity } from "typeorm";

@Entity()
export class Qrcode {
  @PrimaryGeneratedColumn({name: "qrcode_id"})
  id: number;

  @Column()
  qrcodeamount: number;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updateAt: Date;

  @DeleteDateColumn()
  deleatedAt: Date;
}
