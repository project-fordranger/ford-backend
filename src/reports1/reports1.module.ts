import { Module } from '@nestjs/common';
import { Reports1Service } from './reports1.service';
import { Reports1Controller } from './reports1.controller';

@Module({
  controllers: [Reports1Controller],
  providers: [Reports1Service]
})
export class Reports1Module {}
