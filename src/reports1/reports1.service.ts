import { Injectable } from '@nestjs/common';
import { CreateReports1Dto } from './dto/create-reports1.dto';
import { UpdateReports1Dto } from './dto/update-reports1.dto';
import { InjectDataSource } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';

@Injectable()
export class Reports1Service {

  constructor(@InjectDataSource() private dataSource: DataSource) {}
  getProduct() {
    return this.dataSource.query('SELECT * FROM getProduct_info01');
  }
  delOrder() {
    return this.dataSource.query('CALL delOrder');
  }
  sellDW() {
    return this.dataSource.query('SELECT * FROM `order` ORDER BY `order`.`total` DESC');
  }
  // getProductBySearchText(searchText: any) {
  //   return this.dataSource.query('SELECT * FROM getProduct_info01()');
  // }

  create(createReports1Dto: CreateReports1Dto) {
    return 'This action adds a new reports1';
  }

  findAll() {
    return `This action returns all reports1`;
  }

  findOne(id: number) {
    return `This action returns a #${id} reports1`;
  }

  update(id: number, updateReports1Dto: UpdateReports1Dto) {
    return `This action updates a #${id} reports1`;
  }

  remove(id: number) {
    return `This action removes a #${id} reports1`;
  }
}
