import { Test, TestingModule } from '@nestjs/testing';
import { Reports1Service } from './reports1.service';

describe('Reports1Service', () => {
  let service: Reports1Service;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [Reports1Service],
    }).compile();

    service = module.get<Reports1Service>(Reports1Service);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
