import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { Reports1Service } from './reports1.service';
import { CreateReports1Dto } from './dto/create-reports1.dto';
import { UpdateReports1Dto } from './dto/update-reports1.dto';

@Controller('reports1')
export class Reports1Controller {
  constructor(private readonly reports1Service: Reports1Service) {}
  
  @Get('/products')
  getProduct() {
    return this.reports1Service.getProduct();
  }
  @Get('/delorder')
  delOrder() {
    return this.reports1Service.delOrder();
  }
  @Get('/selldw')
  sellDW() {
    return this.reports1Service.sellDW();
  }

  // @Get('/products')
  // getProduct(@Query() 
  // query: { 
  //   lowPrice?: number, 
  //   upperPrice?: number, 
  //   searchText?: string 
  // },) {
  //   if (query.searchText) { 
  //     return this.reports1Service.getProductBySearchText(query.searchText);
  //   }
  //   return this.reports1Service.getProduct();
  // }

  @Post()
  create(@Body() createReports1Dto: CreateReports1Dto) {
    return this.reports1Service.create(createReports1Dto);
  }

  @Get()
  findAll() {
    return this.reports1Service.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.reports1Service.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateReports1Dto: UpdateReports1Dto) {
    return this.reports1Service.update(+id, updateReports1Dto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.reports1Service.remove(+id);
  }
}
