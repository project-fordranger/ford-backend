import { PartialType } from '@nestjs/mapped-types';
import { CreateReports1Dto } from './create-reports1.dto';

export class UpdateReports1Dto extends PartialType(CreateReports1Dto) {}
