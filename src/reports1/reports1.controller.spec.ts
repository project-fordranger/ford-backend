import { Test, TestingModule } from '@nestjs/testing';
import { Reports1Controller } from './reports1.controller';
import { Reports1Service } from './reports1.service';

describe('Reports1Controller', () => {
  let controller: Reports1Controller;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [Reports1Controller],
      providers: [Reports1Service],
    }).compile();

    controller = module.get<Reports1Controller>(Reports1Controller);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
