import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CustomersModule } from './customers/customers.module';
import { Customer } from './customers/entities/customer.entity';
import { Product } from './products/entities/product.entity';
import { ProductsModule } from './products/products.module';
import { User } from './users/entities/user.entity';
import { UsersModule } from './users/users.module';
import { CategoriesModule } from './categories/categories.module';
import { Category } from './categories/entities/category.entity';
import { MaterialsModule } from './materials/materials.module';
import { Material } from './materials/entities/material.entity';
import { EmployeesModule } from './employees/employees.module';
import { Employee } from './employees/entities/employee.entity';
import { AuthModule } from './auth/auth.module';
import { OrdersModule } from './orders/orders.module';
import { Order } from './orders/entities/order.entity';
import { OrderItem } from './orders/entities/order-item';
import { Reports1Module } from './reports1/reports1.module';
import { CashsModule } from './cashs/cashs.module';
import { Cash } from './cashs/entities/cash.entity';
import { CreditsModule } from './credits/credits.module';
import { Creditcard } from './credits/entities/credit.entity';
import { QrcodesModule } from './qrcodes/qrcodes.module';
import { Qrcode } from './qrcodes/entities/qrcode.entity';
import { StoreModule } from './store/store.module';
import { Store } from './store/entities/store.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot(
      // {
      //  type: 'sqlite',
      // database: 'db.sqlite',
      // entities: [Customer, Product, User, Category, Material, Employee, Order, OrderItem, Cash, Creditcard, Qrcode, Store],
      // migrations: [],
      // synchronize: true,
      // }
      {
        type: 'mysql',
        host: 'db4free.net',
        port: 3306,
        username: 'ford_ranger',
        password: 'Pass@1234',
        database: 'ford_ranger_db',
        entities: [Customer, Product, User, Category, Material, Employee, Order, OrderItem, Cash, Creditcard, Qrcode, Store],

        synchronize: true,
      },
      //  {
      //   type: 'mysql',
      //   host: 'playground.informatics.buu.ac.th',
      //   port: 3306,
      //   username: 'user8',
      //   password: '+AEPMP99mqo9YtznDeGNPg==',
      //   database: 'user8',
      //   entities: [Customer, Product, User, Category, Material, Employee, Order, OrderItem, Cash],

      //   synchronize: true,
      //   },

      //  {
      //   type: 'mysql',
      //   host: 'angsila.informatics.buu.ac.th',
      //   port: 3306,
      //   username: 'guest12',
      //   password: 'FH3M53UF',
      //   database: 'guest12',
      //   entities: [Customer, Product, User, Category, Material, Employee, Order, OrderItem, Cash, Creditcard, Qrcode, Store],
      //   synchronize: true,
      //   },

  

    ),
    CustomersModule,
    ProductsModule,
    UsersModule,
    CategoriesModule,
    MaterialsModule,
    EmployeesModule,
    AuthModule,
    OrdersModule,
    Reports1Module,
    CashsModule,
    CreditsModule,
    QrcodesModule,
    StoreModule,

  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) { }
}
