import { Order } from "src/orders/entities/order.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Employee {
  @PrimaryGeneratedColumn({name: "employee_id"})
  id: number;

  @Column()
  name: string;

  @Column()
  address: string;

  @Column()
  tel: string;

  @Column()
  email: string;

  @Column()
  position: string;

  @Column()
  hourly: number;

  @Column()
  timetowork: string;

  @Column()
  timeoff: string;

  @Column()
  salary: number;

  @Column({
    length: '128',
    default: 'No-Image-Placeholder.jpg',
  })
  image: string;

  @OneToMany(() => Order, (order) => order.employee)
  orders: Order[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
