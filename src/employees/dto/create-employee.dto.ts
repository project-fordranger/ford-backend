export class CreateEmployeeDto {
  name: string;

  address: string;

  tel: string;

  email: string;

  position: string;

  hourly: number;

  image = 'No-Image-Placeholder.jpg';
}
