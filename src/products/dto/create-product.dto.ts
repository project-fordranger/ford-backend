import { IsNotEmpty, Length, Min } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(3, 32)
  name: string;

  @IsNotEmpty()
  price: number;  

  // @IsNotEmpty()
  // size: string;

  // @IsNotEmpty()
  // type: string;

  image = 'No-Image-Placeholder.jpg';

  @IsNotEmpty()
  categoryId: number;
}
