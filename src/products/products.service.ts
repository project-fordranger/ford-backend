import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from 'src/categories/entities/category.entity';
import { Like, Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
    @InjectRepository(Category)
    private categoriresRepository: Repository<Category>,
  ) { }


  async create(createProductDto: CreateProductDto) {
    const category = await this.categoriresRepository.findOne({ where: { id: createProductDto.categoryId } });
    const newProduct = new Product();
    newProduct.name = createProductDto.name;
    newProduct.price = createProductDto.price;
    newProduct.image = createProductDto.image;
    newProduct.category = category;
    return this.productRepository.save(newProduct);
  }

  findByCategory(id:number) {
    return this.productRepository.find({where: {categoryId: id}});
  }

  async findAll(query): Promise<Paginate> {
    const page = query.page || 1;
    const take = query.take || 100;
    const skip = (page - 1) * take;
    const keyword = query.keyword || ''
    const orderBy = query.orderBy || 'name'
    const order = query.order || 'DESC'
    const currentPage = page
    const [result , total] = await this.productRepository.findAndCount({
      relations: ['category'],
      where: { name: Like(`%${keyword}%`)},
      order: { [orderBy]: order},
      take: take,
      skip: skip
    });
    const lastPage = Math.ceil(total/take)
    return {
      data: result,
      count: total,
      currentPage: currentPage,
      lastPage: lastPage
    }
  }

  async findOne(id: number) {
    const product = await this.productRepository.findOne({ where: { id: id } });
    if (!product) {
      throw new NotFoundException();
    }
    return product;
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const product = await this.productRepository.findOneBy({ id: id });
    if (!product) {
      throw new NotFoundException();
    }
    const updateCustomer = { ...product, ...updateProductDto };
    return this.productRepository.save(updateCustomer);
  }

  async remove(id: number) {
    const product = await this.productRepository.findOneBy({ id: id });
    if (!product) {
      throw new NotFoundException();
    }
    return this.productRepository.softRemove(product);
  }
}
