import { IsNotEmpty } from "class-validator";

export class CreateMaterialDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  unit: number;

  @IsNotEmpty()
  expire: string;

  @IsNotEmpty()
  stockIn: string;

  image = 'No-Image-Placeholder.jpg';
}
