import { Column, CreateDateColumn, DeleteDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Material {
  @PrimaryGeneratedColumn({name: "material_id"})
  id: number;

  @Column()
  name: string;

  @Column()
  unit: number;

  @Column()
  expire: string;

  @Column()
  stockIn: string;

  @Column()
  amountLeft: number;

  @Column()
  minimun: number;

  @Column({
    length: '128',
    default: 'No-Image-Placeholder.jpg',
  })
  image: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
