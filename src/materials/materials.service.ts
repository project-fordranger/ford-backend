import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateMaterialDto } from './dto/create-material.dto';
import { UpdateMaterialDto } from './dto/update-material.dto';
import { Material } from './entities/material.entity';

@Injectable()
export class MaterialsService {
  constructor(
    @InjectRepository(Material)
    private MaterialsRepository: Repository<Material>,
  ) {}

  create(createMaterialDto: CreateMaterialDto) {
    return this.MaterialsRepository.save(createMaterialDto);
  }

  findAll() {
    return this.MaterialsRepository.find({
      order: {
        id: 'DESC'
    }
  });
  }

  async findOne(id: number) {
    const material = await this.MaterialsRepository.findOne({
      where: { id: id },
    });
    if (!material) {
      throw new NotFoundException();
    }
    return material;
  }

  async update(id: number, updateMaterialDto: UpdateMaterialDto) {
    const material = await this.MaterialsRepository.findOneBy({ id: id });
    if (!material) {
      throw new NotFoundException();
    }
    const updateCustomer = { ...material, ...updateMaterialDto };
    return this.MaterialsRepository.save(updateCustomer);
  }

  async remove(id: number) {
    const material = await this.MaterialsRepository.findOneBy({ id: id });
    if (!material) {
      throw new NotFoundException();
    }
    return this.MaterialsRepository.softRemove(material);
  }
}
