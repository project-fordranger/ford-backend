import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { Category } from './entities/category.entity';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectRepository(Category)
    private categoriresRepository: Repository<Category>,
  ) { }
  create(createCategoryDto: CreateCategoryDto) {
    return this.categoriresRepository.save(createCategoryDto);
  }

  findAll() {
    return this.categoriresRepository.find();
  }

  findOne(id: number) {
    return `This action returns a #${id} category`;
  }

  async update(id: number, updateCategoryDto: UpdateCategoryDto) {
    const category = await this.categoriresRepository.findOneBy({ id: id });
    if (!category) {
      throw new NotFoundException();
    }
    const updateCustomer = { ...category, ...updateCategoryDto };
    return this.categoriresRepository.save(updateCustomer);
  }

  async remove(id: number) {
    const category = await this.categoriresRepository.findOneBy({ id: id });
    return this.categoriresRepository.softRemove(category);
  }
}
