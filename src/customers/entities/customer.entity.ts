import { Order } from 'src/orders/entities/order.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class Customer {
  @PrimaryGeneratedColumn({name : "customer_id"})
  id: number;

  @Column()
  age: number;

  @Column()
  name: string;

  @Column()
  tel: string;

  @Column()
  gender: string;

  // @Column()
  // cuscode: string;

  @OneToMany(() => Order, (order) => order.customer)
  orders: Order[];

  @Column()
  points: number;
  
  // @Column({
  //   length: '128',
  //   default: 'No-Image-Placeholder.jpg',
  // })
  // image: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updateAt: Date;

  @DeleteDateColumn()
  deleatedAt: Date;
}
