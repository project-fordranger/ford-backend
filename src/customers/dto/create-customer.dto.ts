import { IsNotEmpty, Min, MinLength, minLength } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  age: number;

  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  tel: string;

  @IsNotEmpty()
  gender: string;

  // @IsNotEmpty()
  // cuscode: string;

  // image = 'No-Image-Placeholder.jpg';
  @Min(0)
  points: number;
}
