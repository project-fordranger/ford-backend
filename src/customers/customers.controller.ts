import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
  Res,
  Query,
} from '@nestjs/common';
import { CustomersService } from './customers.service';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
// import { FileInterceptor } from '@nestjs/platform-express';
// import { diskStorage } from 'multer';
// import { v4 as uuidv4 } from 'uuid';
// import { extname } from 'path';
// import { Response } from 'express';
@Controller('customers')
export class CustomersController {
  constructor(private readonly customersService: CustomersService) {}

  @Post()
  // @UseInterceptors(
  //   FileInterceptor('file', {
  //     storage: diskStorage({
  //       destination: './customer_images',
  //       filename: (req, file, cb) => {
  //         const name = uuidv4();
  //         return cb(null, name + extname(file.originalname));
  //       },
  //     }),
  //   }),
  // )
  create(
    @Body() createCustomerDto: CreateCustomerDto
    // ,@UploadedFile() file: Express.Multer.File,
  ) {
    // createCustomerDto.image = file.filename;
    return this.customersService.create(createCustomerDto);
  }

  @Get()
  findAll(
    @Query() query,
  ) {
    return this.customersService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.customersService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateCustomerDto: UpdateCustomerDto,
  ) {
    return this.customersService.update(+id, updateCustomerDto);
  }

  // @Patch(':id/image')
  // @UseInterceptors(
  //   FileInterceptor('file', {
  //     storage: diskStorage({
  //       destination: './customer_images',
  //       filename: (req, file, cb) => {
  //         const name = uuidv4();
  //         return cb(null, name + extname(file.originalname));
  //       },
  //     }),
  //   }),
  // )
  // updateImage(
  //   @Param('id') id: string,
  //   @UploadedFile() file: Express.Multer.File,
  // ) {
  //   return this.customersService.update(+id, { image: file.filename });
  // }

  // @Get(':id/image')
  // async getImage(@Param('id') id: string, @Res() res: Response) {
  //   const customer = await this.customersService.findOne(+id);
  //   res.sendFile(customer.image, { root: './customer_images' });
  // }

  // @Get('image/:imageFile')
  // async getImageByFileName(
  //   @Param('imageFile') imageFile: string,
  //   @Res() res: Response,
  // ) {
  //   res.sendFile(imageFile, { root: './customer_images' });
  // }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.customersService.remove(+id);
  }
}
